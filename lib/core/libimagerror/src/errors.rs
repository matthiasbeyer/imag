//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 the imag contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use thiserror::Error;

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("IO error")]
    IO(#[from] ::std::io::Error),

    #[error("Unknown IO error")]
    UnknownIO,

    #[error("Locking error")]
    LockError,

    #[error("Locking error")]
    PoisonError,

    #[error("UTF8 error")]
    UTF8Error(#[from] std::str::Utf8Error),

    #[error("Error in external process")]
    ExternalProcessError,

    #[error("File Error")]
    FileError,

    #[error("File not copied")]
    FileNotCopied,

    #[error("File not created")]
    FileNotCreated,

    #[error("File not found")]
    FileNotFound,

    #[error("Fail not removed")]
    FileNotRemoved,

    #[error("Fail not renamed")]
    FileNotRenamed,

    #[error("File not seeked")]
    FileNotSeeked,

    #[error("File not written")]
    FileNotWritten,

    #[error("Directory not created")]
    DirNotCreated,


    #[error("Formatting error")]
    FormatError(#[from] std::fmt::Error),


    #[error("ID is locked")]
    IdLocked,

    #[error("Error while converting values")]
    ConversionError,


    #[error("Entry exists already: {0}")]
    EntryAlreadyExists(String),

    #[error("Entry not found: {0}")]
    EntryNotFound(String),

    #[error("Entry header error")]
    EntryHeaderError,

    #[error("Entry header type error")]
    EntryHeaderTypeError,

    #[error("Entry header type error at '{0}', expected '{1}'")]
    EntryHeaderTypeError2(&'static str, &'static str),

    #[error("Entry header read error")]
    EntryHeaderReadError,

    #[error("Entry header write error")]
    EntryHeaderWriteError,

    #[error("Entry header field missing: {0}")]
    EntryHeaderFieldMissing(&'static str),


    #[error("Toml deserialization error")]
    TomlDeserError,

    #[error("Toml querying error")]
    TomlQueryError,

}

