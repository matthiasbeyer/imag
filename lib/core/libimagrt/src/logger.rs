//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::io::Write;
use std::io::stderr;
use std::collections::BTreeMap;
use std::sync::Arc;
use std::sync::Mutex;
use std::ops::Deref;

use anyhow::Result;
use anyhow::Error;

use clap::ArgMatches;
use log::{Log, Level, Record, Metadata};
use toml::Value;
use toml_query::read::TomlValueReadExt;
use toml_query::read::TomlValueReadTypeExt;
use handlebars::Handlebars;

#[derive(Debug)]
enum LogDestination {
    Stderr,
    File(Arc<Mutex<::std::fs::File>>),
}

impl Default for LogDestination {
    fn default() -> LogDestination {
        LogDestination::Stderr
    }
}

/// Logger implementation for `log` crate.
#[derive(Debug)]
pub struct ImagLogger<'rc> {
    global_loglevel     : Level,

    #[allow(unused)]
    global_destinations : Vec<LogDestination>,

    handlebars: Handlebars<'rc>,
}

impl<'rc> ImagLogger<'rc> {

    /// Create a new ImagLogger object with a certain level
    pub fn new(matches: &ArgMatches, config: Option<&Value>) -> Result<ImagLogger<'rc>> {
        let mut handlebars = Handlebars::new();

        handlebars.register_escape_fn(::handlebars::no_escape);

        ::libimaginteraction::format::register_all_color_helpers(&mut handlebars);
        ::libimaginteraction::format::register_all_format_helpers(&mut handlebars);

        {
            use self::log_lvl_aggregate::*;
            aggregate::<Trace>(&mut handlebars, config, "TRACE")?;
            aggregate::<Debug>(&mut handlebars, config, "DEBUG")?;
            aggregate::<Info>(&mut handlebars, config, "INFO")?;
            aggregate::<Warn>(&mut handlebars, config, "WARN")?;
            aggregate::<Error>(&mut handlebars, config, "ERROR")?;
        }

        Ok(ImagLogger {
            global_loglevel     : aggregate_global_loglevel(matches, config)?,
            global_destinations : aggregate_global_destinations(config)?,
            handlebars,
        })
    }

    pub fn global_loglevel(&self) -> Level {
        self.global_loglevel
    }

}

impl<'rc> Log for ImagLogger<'rc> {

    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.global_loglevel
    }

    fn flush(&self) {
        // nothing?
    }

    fn log(&self, record: &Record) {
        let mut data = BTreeMap::new();

        {
            data.insert("level",        format!("{}", record.level()));
            data.insert("module_path",  String::from(record.module_path().unwrap_or("<modulepath unknown>")));
            data.insert("file",         String::from(record.file().unwrap_or("<file unknown>")));
            data.insert("line",         format!("{}", record.line().unwrap_or(0)));
            data.insert("target",       String::from(record.target()));
            data.insert("message",      format!("{}", record.args()));
        }

        let logtext = self
            .handlebars
            .render(&format!("{}", record.level()), &data)
            .unwrap_or_else(|e| format!("Failed rendering logging data: {:?}\n", e));

        let log_to_destination = |d: &LogDestination| match *d {
            LogDestination::Stderr => {
                let _ = writeln!(stderr(), "{}", logtext);
            },
            LogDestination::File(ref arc_mutex_logdest) => {
                // if there is an error in the lock, we cannot do anything. So we ignore it here.
                let _lock = arc_mutex_logdest
                    .deref()
                    .lock()
                    .map(|mut logdest| {
                        writeln!(logdest, "{}", logtext)
                    });
            }
        };

        if self.global_loglevel >= record.level() {
            // Yes, we log
            for d in self.global_destinations.iter() {
                // If there's an error, we cannot do anything, can we?
                log_to_destination(&d);
            }
        }
    }
}

fn match_log_level_str(s: &str) -> Result<Level> {
    match s {
        "trace" => Ok(Level::Trace),
        "debug" => Ok(Level::Debug),
        "info"  => Ok(Level::Info),
        "warn"  => Ok(Level::Warn),
        "error" => Ok(Level::Error),
        lvl     => Err(anyhow!("Invalid logging level: {}", lvl)),
    }
}

fn aggregate_global_loglevel(matches: &ArgMatches, config: Option<&Value>) -> Result<Level>
{
    fn get_arg_loglevel(matches: &ArgMatches) -> Result<Option<Level>> {
        if matches.is_present("debugging") {
            return Ok(Some(Level::Debug))
        }

        match matches.value_of("verbosity") {
            Some(v) => match_log_level_str(v).map(Some),
            None    => if matches.is_present("verbosity") {
                Ok(Some(Level::Info))
            } else {
                Ok(None)
            },
        }
    }

    if let Some(cfg) = config {
        let cfg_loglevel = cfg
            .read_string("imag.logging.level")?
            .ok_or_else(|| anyhow!("Global log level config missing"))
            .and_then(|s| match_log_level_str(&s))?;

        if let Some(cli_loglevel) = get_arg_loglevel(matches)? {
            if cli_loglevel > cfg_loglevel {
                return Ok(cli_loglevel)
            }
        }

        Ok(cfg_loglevel)

    } else {
        get_arg_loglevel(matches).map(|o| o.unwrap_or(Level::Info))
    }
}

fn translate_destination(raw: &str) -> Result<LogDestination> {
    use std::fs::OpenOptions;

    match raw {
        "-" => Ok(LogDestination::Stderr),
        other => {
            OpenOptions::new()
                .append(true)
                .create(true)
                .open(other)
                .map(Mutex::new)
                .map(Arc::new)
                .map(LogDestination::File)
                .map_err(Error::from)
        }
    }
}

fn aggregate_global_destinations(config: Option<&Value>) -> Result<Vec<LogDestination>> {
    match config {
        None      => Ok(vec![LogDestination::default()]),
        Some(cfg) => cfg
            .read("imag.logging.destinations")?
            .ok_or_else(|| anyhow!("Global log destination config missing"))?
            .as_array()
            .ok_or_else(|| {
                let msg = "Type error at 'imag.logging.destinations', expected 'Array'";
                anyhow!(msg)
            })
            .and_then(|raw| {
                raw.iter()
                    .map(|val| {
                        val.as_str()
                            .ok_or_else(|| anyhow!("Type error at 'imag.logging.modules.<mod>.destinations', expected Array<String>"))
                            .map_err(Error::from)
                            .and_then(|s| translate_destination(s))
                    })
                    .collect::<Result<Vec<LogDestination>>>()
            }),
    }
}

mod log_lvl_aggregate {
    use anyhow::Result;
    use anyhow::Error as E;
    use anyhow::Context;
    use toml::Value;
    use toml_query::read::TomlValueReadTypeExt;
    use handlebars::Handlebars;

    macro_rules! aggregate_global_format_with {
        ($t:ident, $read_str:expr) => {
            pub struct $t;
            impl LogLevelAggregator for $t {
                fn aggregate(config: Option<&Value>) -> Result<String> {
                    config.ok_or_else(|| {
                        E::from(anyhow!(concat!("Config missing: Logging format: ", stringify!($t))))
                    })?
                    .read_string($read_str)?
                    .ok_or_else(|| {
                        E::from(anyhow!(concat!("Config missing: Logging format: ", stringify!($t))))
                    })
                }
            }
        };
    }

    pub trait LogLevelAggregator {
        fn aggregate(config: Option<&Value>) -> Result<String>;
    }

    pub fn aggregate<T: LogLevelAggregator>(hb: &mut Handlebars, config: Option<&Value>, lvlstr: &str)
        -> Result<()>
    {
        hb.register_template_string(lvlstr, T::aggregate(config)?)
            .map_err(E::from)
            .context(anyhow!("Handlebars template error"))
            .map_err(E::from)
    }

    aggregate_global_format_with!(Trace, "imag.logging.format.trace");
    aggregate_global_format_with!(Debug, "imag.logging.format.debug");
    aggregate_global_format_with!(Info, "imag.logging.format.info");
    aggregate_global_format_with!(Warn, "imag.logging.format.warn");
    aggregate_global_format_with!(Error, "imag.logging.format.error");

}

