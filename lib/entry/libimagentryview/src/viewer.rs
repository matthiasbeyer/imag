//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::io::Write;
use std::ops::Deref;

use libimagstore::store::Entry;
use libimagstore::store::FileLockEntry;

use anyhow::Result;

pub trait Viewer {

    fn view_entry<W>(&self, e: &Entry, sink: &mut W) -> Result<()>
        where W: Write;

    fn view_entries<I, E, W>(&self, entries: I, sink: &mut W) -> Result<()>
        where I: Iterator<Item = E>,
              E: Deref<Target = Entry>,
              W: Write
    {
        for entry in entries {
            self.view_entry(entry.deref(), sink)?;
        }
        Ok(())
    }
}

/// Extension for all iterators, so that an iterator can be viewed with:
///
/// ```ignore
///     iter.view::<Viewer, _>(&mut sink)
/// ```
///
pub trait ViewFromIter {
    fn view<V, W>(self, sink: &mut W) -> Result<()>
        where V: Viewer + Default,
              W: Write;

    fn view_with<V, W>(self, v: V, sink: &mut W) -> Result<()>
        where V: Viewer,
              W: Write;
}

impl<I, E> ViewFromIter for I
    where I: Iterator<Item = E>,
          E: Deref<Target = Entry>
{
    fn view<V, W>(self, sink: &mut W) -> Result<()>
        where V: Viewer + Default,
              W: Write
    {
        self.view_with(V::default(), sink)
    }

    fn view_with<V, W>(self, v: V, sink: &mut W) -> Result<()>
        where V: Viewer,
              W: Write
    {
        v.view_entries(self, sink)
    }
}

pub trait IntoViewIter<V, W, F, T>
    where Self: Iterator<Item = T> + Sized,
          V: Viewer,
          W: Write,
          F: Fn(&T) -> Option<&FileLockEntry>,
          T: Sized,
{
    fn view_all_if(self, v: V, sink: &mut W, func: F) -> ViewIter<Self, V, W, F, T> {
        ViewIter {
            inner: self,
            viewer: v,
            func,
            sink,
        }
    }
}

impl<I, V, W, F, T> IntoViewIter<V, W, F, T> for I
    where I: Iterator<Item = T>,
          V: Viewer,
          W: Write,
          F: Fn(&T) -> Option<&FileLockEntry>,
          T: Sized,
{
    // default impl
}


pub struct ViewIter<'a, I, V, W, F, T>
    where I: Iterator<Item = T>,
          V: Viewer,
          W: Write,
          F: Fn(&T) -> Option<&FileLockEntry>,
          T: Sized,
{
    inner: I,
    viewer: V,
    func: F,
    sink: &'a mut W
}

impl<'a, I, V, W, F, T> Iterator for ViewIter<'a, I, V, W, F, T>
    where I: Iterator<Item = T>,
          V: Viewer,
          W: Write,
          F: Fn(&T) -> Option<&FileLockEntry>,
          T: Sized,
{
    type Item = Result<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(next) = self.inner.next() {
            if let Some(entry) = (self.func)(&next) {
                let r = self.viewer.view_entry(&entry, self.sink);
                trace!("Viewing resulted in {:?}", r);
                if let Err(e) = r {
                    if let Some(ioerr) = e.downcast_ref::<::std::io::Error>() {
                        if ioerr.kind() == ::std::io::ErrorKind::BrokenPipe {
                            trace!("Stopping iteration, because of broken pipe error!");
                            return None
                        }
                    }

                    return Some(Err(e))
                }
            }

            return Some(Ok(next))
        }

        None
    }
}

