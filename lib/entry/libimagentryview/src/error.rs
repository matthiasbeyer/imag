//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::fmt::Display;

#[derive(Debug)]
pub enum Error {
    Io(::std::io::Error),
    Other(::anyhow::Error),
}

impl Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
        match self {
            Error::Io(e)    => write!(f, "{}", e),
            Error::Other(e) => write!(f, "{}", e),
        }
    }
}

impl From<::std::io::Error> for Error {
    fn from(ioe: ::std::io::Error) -> Self {
        Error::Io(ioe)
    }
}

impl From<::anyhow::Error> for Error {
    fn from(fe: ::anyhow::Error) -> Self {
        Error::Other(fe)
    }
}

impl Into<::anyhow::Error> for Error {
    fn into(self) -> ::anyhow::Error {
        match self {
            Error::Io(e)    => ::anyhow::Error::from(e),
            Error::Other(e) => e,
        }
    }
}

