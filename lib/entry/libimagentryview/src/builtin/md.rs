//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::io::Write;

use libimagstore::store::Entry;
use libimagrt::runtime::Runtime;

use mdcat::{ResourceAccess, TerminalCapabilities, TerminalSize};
use pulldown_cmark::Parser;
use syntect::parsing::SyntaxSet;
use anyhow::Result;
use anyhow::format_err;

use crate::viewer::Viewer;

pub struct MarkdownViewer<'a>(&'a Runtime<'a>);

impl<'a> MarkdownViewer<'a> {
    pub fn new(rt: &'a Runtime) -> Self {
        MarkdownViewer(rt)
    }
}

impl<'a> Viewer for MarkdownViewer<'a> {
    fn view_entry<W>(&self, e: &Entry, sink: &mut W) -> Result<()>
        where W: Write
    {
        let parser   = Parser::new(e.get_content());
        let base_dir = self.0.rtp();
        let settings = mdcat::Settings {
            terminal_capabilities: TerminalCapabilities::ansi(),
            syntax_set: SyntaxSet::load_defaults_newlines(),
            resource_access: ResourceAccess::LocalOnly,
            terminal_size: TerminalSize::detect().unwrap_or(TerminalSize {
                width: 80,
                height: 20,
            }),
        };

        ::mdcat::push_tty(&settings,
                          sink,
                          base_dir,
                          parser)
        .map_err(|_| format_err!("Failed while formatting markdown"))
    }
}

