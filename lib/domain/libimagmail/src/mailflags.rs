//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::fmt::{Display, Result as FmtResult, Formatter};
use std::str::FromStr;

use anyhow::Result;
use anyhow::Error;

/// Message flags
///
/// As defined by https://cr.yp.to/proto/maildir.html with strong typing
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum MailFlag {
    /// Flag "P" (passed): the user has resent/forwarded/bounced this message to someone else.
    Passed,

    /// Flag "R" (replied): the user has replied to this message.
    Replied,

    /// Flag "S" (seen): the user has viewed this message, though perhaps he didn't read all the way through it.
    Seen,

    /// Flag "T" (trashed): the user has moved this message to the trash; the trash will be emptied by a later user action.
    Trashed,

    /// Flag "D" (draft): the user considers this message a draft; toggled at user discretion.
    Draft,

    /// Flag "F" (flagged): user-defined flag; toggled at user discretion.
    Flagged,
}

impl MailFlag {
    pub fn as_char(self) -> char {
        match self {
            MailFlag::Passed  => 'P',
            MailFlag::Replied => 'R',
            MailFlag::Seen    => 'S',
            MailFlag::Trashed => 'T',
            MailFlag::Draft   => 'D',
            MailFlag::Flagged => 'F',
        }
    }
}

impl FromStr for MailFlag {
    type Err = Error;

    fn from_str(s: &str) -> Result<MailFlag> {
        match s {
            "P" => Ok(MailFlag::Passed),
            "R" => Ok(MailFlag::Replied),
            "S" => Ok(MailFlag::Seen),
            "T" => Ok(MailFlag::Trashed),
            "D" => Ok(MailFlag::Draft),
            "F" => Ok(MailFlag::Flagged),
            _   => Err(anyhow!("Unknown message flag: '{}'", s)),
        }
    }
}

impl Display for MailFlag {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let s = match self {
            MailFlag::Passed  => "Passed",
            MailFlag::Replied => "Replied",
            MailFlag::Seen    => "Seen",
            MailFlag::Trashed => "Trashed",
            MailFlag::Draft   => "Draft",
            MailFlag::Flagged => "Flagged",
        };

        write!(f, "{}", s)
    }
}

