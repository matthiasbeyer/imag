## The Approach {#sec:intro:approach}

The approach "imag" takes on solving this problem is to store content in a
"store" and persisting content in a unified way.
Meta-information is attached to the content which can be used to store
structured data.
This can be used to implement a variety of "domain modules" using the store.
While content is stored in _one_ place, imag does not duplicate content.
imag does not copy or move icalendar files, emails, vcard files, music or
movies to the store, but tries to remember the actual files are and stores
meta-information about them in the store.

Detailed explanation on this approach follows in the chapters of this work.


