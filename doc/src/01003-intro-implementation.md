## Implementation {#sec:intro:implementation}

The program is written in the Rust programming language.

The program consists of libraries which can be re-used by other projects
to implement and adapt imag functionality. An external program may use a
library of the imag distribution to store content in the store of imag and
make it visible to imag this way.

This is a technical detail a user does not necessarily need to know, but as imag
is intended for power-users anyways, we would say it fits here.


