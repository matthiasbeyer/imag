## The Problem {#sec:intro:problem}

The problem this project tries to solve is to provide a modular commandline
application for personal information management.

It targets "power users" or "commandline users", uses plain text as a storage
format and tries to be as scriptable as possible.
imag offers the ability to link data from different "PIM aspects" (such as
"diary", "contacts" and "bookmark" for example).

One major goal of imag is to make the PIM data traverseable and queryable.
For example: a wiki article can be linked to an appointment which is linked to a
todo which is linked to a note which is linked to a contact.

imag wants to offer an all-in-one scriptable modular commandline personal
information management suite for all PIM aspects one could possibly think of.
Because imag uses plain text (TOML headers for structured data and plain text
which can be rendered using markdown, for example, for continuous text)
the user is always able to access their data without the imag tools at hand.


