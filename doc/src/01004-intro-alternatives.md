## Alternative Projects {#sec:intro:alternatives}

imag is not the only project which tries to solve that particular problem. For
example there is
[org mode](https://orgmode.org)
for the [emacs](https://www.gnu.org/software/emacs/) text editor.
There is also [zim](http://zim-wiki.org/), a desktop wiki editor which is
intended to be used for a personal wiki.

The difference between imag and the mentioned projects is:
* emacs orgmode is (from what I know and see) for _orgabizing_ things. imag is
  intended not only for organizing, but also for recording, tracking and
  querying.
* zim is a wiki, which could be used for PIM but is not specialized for it.
  Recording habits might be possible, but not that simple as with imag

imag is not there
yet, though. Some parts can be used, though it is far away from being feature-complete.

In addition: imag is text-editor independent and other tools than imag might be
used to access data stored in the imag store.
For example, one could "grep", "awk" and "sed" entries without much hassle and
even write bash scripts for automatically filling imag entries with data.



