//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use clap::ArgMatches;
use chrono::naive::NaiveDate;
use chrono::naive::NaiveDateTime;
use anyhow::Error;
use anyhow::Result;


use libimagrt::runtime::Runtime;
use libimagtimetrack::tag::TimeTrackingTag;
use libimagtimetrack::store::TimeTrackStore;

const DATE_TIME_PARSE_FMT : &str    = "%Y-%m-%dT%H:%M:%S";
const DATE_PARSE_FMT : &str         = "%Y-%m-%d";

pub fn track(rt: &Runtime) -> Result<()> {
    let (_, cmd) = rt.cli().subcommand();
    let cmd = cmd.unwrap(); // checked in main()

    // Gets the appropriate time from the commandline or None on error (errors already logged, so
    // callee can directly return in case of error
    fn get_time(cmd: &ArgMatches, clap_name: &str) -> Result<Option<NaiveDateTime>> {
        match cmd.value_of(clap_name) {
            None        => bail!("Not specified in commandline: {}", clap_name),
            Some("now") => Ok(Some(::chrono::offset::Local::now().naive_local())),
            Some(els)   => match NaiveDateTime::parse_from_str(els, DATE_TIME_PARSE_FMT) {
                Ok(ndt) => Ok(Some(ndt)),
                Err(_)  => Ok(Some(NaiveDate::parse_from_str(els, DATE_PARSE_FMT)?.and_hms(0, 0, 0))),
            }
        }
    }

    let start = get_time(&cmd, "start-time")?.ok_or_else(|| anyhow!("No start-time"))?;
    let stop  = get_time(&cmd, "end-time")?.ok_or_else(|| anyhow!("No end-time"))?;

    cmd.values_of("tags")
        .unwrap() // enforced by clap
        .map(String::from)
        .map(TimeTrackingTag::from)
        .map(|ttt| {
            let entry = rt.store().create_timetracking(&start, &stop, &ttt)?;
            rt.report_touched(entry.get_location()).map_err(Error::from)
        })
        .collect::<Result<Vec<_>>>()
        .map(|_| ())
}

