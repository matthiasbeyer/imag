//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::str::FromStr;

use filters::filter::Filter;
use chrono::NaiveDateTime;
use anyhow::Result;
use anyhow::Error;
use resiter::Filter as RFilter;
use resiter::Map;
use resiter::AndThen;

use libimagrt::runtime::Runtime;
use libimagtimetrack::tag::TimeTrackingTag;
use libimagtimetrack::store::*;
use libimagtimetrack::iter::filter::has_end_time;
use libimagtimetrack::iter::filter::has_one_of_tags;
use libimagtimetrack::timetracking::TimeTracking;

pub fn stop(rt: &Runtime) -> Result<()> {
    let (_, cmd) = rt.cli().subcommand();
    let cmd = cmd.unwrap(); // checked in main()

    let stop_time = match cmd.value_of("stop-time") {
        None | Some("now") => Ok(::chrono::offset::Local::now().naive_local()),
        Some(ndt)          => NaiveDateTime::from_str(ndt).map_err(Error::from),
    }?;

    let tags = cmd.values_of("tags").map(|tags| {
        tags.map(String::from)
            .map(TimeTrackingTag::from)
            .collect()
    });

    let tags : Vec<TimeTrackingTag> = match tags {
        Some(s) => s,
        None => {
            // Get all timetrackings which do not have an end datetime.
            rt.store()
                .get_timetrackings()?
                .and_then_ok(|t| Ok((t.get_end_datetime()?.is_none(), t)))
                .filter_ok(|tpl| (*tpl).0)
                .map_ok(|tpl| tpl.1)
                .and_then_ok(|t| t.get_timetrack_tag())
                .collect::<Result<Vec<_>>>()?
        }
    };


    let filter = has_end_time.not().and(has_one_of_tags(&tags));
    rt
        .store()
        .get_timetrackings()?

        // Filter all timetrackings for the ones that are not yet ended.
        .filter_ok(|e| filter.filter(e))

        // for each of these timetrackings, end them
        // for each result, print the backtrace (if any)
        .and_then_ok(|mut elem| {
            elem.set_end_datetime(stop_time)?;
            debug!("Setting end time worked: {:?}", elem);
            rt.report_touched(elem.get_location()).map_err(Error::from)
        })
        .collect::<Result<Vec<_>>>()
        .map(|_| ())
}

