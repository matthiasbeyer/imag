//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use anyhow::Result;

use anyhow::Error;
use resiter::AndThen;
use resiter::IterInnerOkOrElse;

use libimagdiary::diary::Diary;
use libimagdiary::viewer::DiaryViewer;
use libimagrt::runtime::Runtime;
use libimagstore::iter::get::StoreIdGetIteratorExtension;
use libimagentryview::viewer::Viewer;

use crate::util::get_diary_name;

pub fn view(rt: &Runtime) -> Result<()> {
    let diaryname   = get_diary_name(rt).ok_or_else(|| anyhow!("No diary name"))?;
    let hdr         = rt.cli().subcommand_matches("view").unwrap().is_present("show-header");
    let out         = rt.stdout();
    let mut outlock = out.lock();
    let viewer      = DiaryViewer::new(hdr);

    Diary::entries(rt.store(), &diaryname)?
        .into_get_iter(rt.store())
        .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
        .and_then_ok(|e| viewer.view_entry(&e, &mut outlock).map_err(Error::from).map(|_| e))
        .and_then_ok(|e| rt.report_touched(e.get_location()).map_err(Error::from))
        .collect()
}

