//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::io::Write;

use anyhow::Result;

use anyhow::Error;
use resiter::AndThen;

use libimagdiary::diary::Diary;
use libimagrt::runtime::Runtime;
use libimagdiary::diaryid::DiaryId;
use libimagdiary::diaryid::FromStoreId;
use libimagstore::storeid::IntoStoreId;

use crate::util::get_diary_name;

pub fn list(rt: &Runtime) -> Result<()> {
    let diaryname = get_diary_name(rt)
        .ok_or_else(|| anyhow!("No diary selected. Use either the configuration file or the commandline option"))?;

    let mut ids = Diary::entries(rt.store(), &diaryname)?
        .and_then_ok(|id| DiaryId::from_storeid(&id))
        .collect::<Result<Vec<_>>>()?;

    ids.sort_by_key(|id| {
        [id.year() as u32, id.month(), id.day(), id.hour(), id.minute(), id.second()]
    });

    ids.into_iter()
        .map(IntoStoreId::into_storeid)
        .and_then_ok(|id| {
            rt.report_touched(&id)?;

            if !rt.output_is_pipe() {
                writeln!(rt.stdout(), "{}", id).map_err(Error::from)
            } else {
                Ok(())
            }
        })
        .collect()
}

