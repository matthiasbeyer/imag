//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use chrono::naive::NaiveDateTime as NDT;
use anyhow::Result;


use libimagdiary::diaryid::DiaryId;
use libimagrt::runtime::Runtime;
use libimagtimeui::datetime::DateTime;
use libimagtimeui::parse::Parse;
use libimagstore::storeid::IntoStoreId;

use crate::util::get_diary_name;

pub fn delete(rt: &Runtime) -> Result<()> {
    use libimaginteraction::ask::ask_bool;

    let diaryname = get_diary_name(rt)
        .ok_or_else(|| anyhow!("No diary selected. Use either the configuration file or the commandline option"))?;

    let to_del_location = rt
        .cli()
        .subcommand_matches("delete")
        .unwrap()
        .value_of("datetime")
        .map(|dt| { debug!("DateTime = {:?}", dt); dt })
        .and_then(DateTime::parse)
        .map(Into::into)
        .ok_or_else(|| anyhow!("Not deleting entries: missing date/time specification"))
        .and_then(|dt: NDT| DiaryId::from_datetime(diaryname.clone(), dt).into_storeid())
        .and_then(|id| rt.store().retrieve(id))?
        .get_location()
        .clone();

    let mut input  = rt.stdin().ok_or_else(|| anyhow!("No input stream. Cannot ask for permission"))?;
    let mut output = rt.stdout();

    if !ask_bool(&format!("Deleting {:?}", to_del_location), Some(true), &mut input, &mut output)? {
        info!("Aborting delete action");
        return Ok(());
    }

    rt.report_touched(&to_del_location)?;
    rt.store().delete(to_del_location)
}

