//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::collections::BTreeMap;

use anyhow::Result;
use anyhow::Error;

use handlebars::Handlebars;
use clap::ArgMatches;
use chrono::NaiveDateTime;
use toml_query::read::TomlValueReadTypeExt;

use libimagrt::runtime::Runtime;
use libimagstore::store::Entry;
use libimagtodo::entry::Todo;
use libimagutil::date::datetime_to_string;

pub fn get_dt_str(d: Result<Option<NaiveDateTime>>, s: &str) -> Result<String> {
    Ok(d?
       .map(|v| datetime_to_string(&v))
       .unwrap_or_else(|| s.to_string()))
}

pub fn get_todo_print_format<'rc>(config_value_path: &'static str, rt: &Runtime, scmd: &ArgMatches) -> Result<Handlebars<'rc>> {
    let fmt = match scmd.value_of("format").map(String::from) {
        Some(s) => Ok(s),
        None => {
            rt.config()
                .ok_or_else(|| anyhow!("No configuration file"))?
                .read_string(config_value_path)
                .map_err(Error::from)?
                .ok_or_else(|| anyhow!("Configuration '{}' does not exist", config_value_path))
        }
    }?;

    let mut hb = Handlebars::new();
    hb.register_template_string("format", fmt)?;

    hb.register_escape_fn(::handlebars::no_escape);
    ::libimaginteraction::format::register_all_color_helpers(&mut hb);
    ::libimaginteraction::format::register_all_format_helpers(&mut hb);
    Ok(hb)
}

pub fn build_data_object_for_handlebars(i: usize, todo: &Entry) -> Result<BTreeMap<&'static str, String>> {
    let mut data = BTreeMap::new();

    data.insert("i", format!("{}", i));
    let uuid     = todo.get_uuid().map_err(Error::from)?.to_string();
    let status   = todo.get_status().map_err(Error::from)?;
    let status   = status.as_str().to_string();
    let text     = todo.get_content().to_string();
    let sched    = get_dt_str(todo.get_scheduled(), "Not scheduled")?;
    let hidden   = get_dt_str(todo.get_hidden(), "Not hidden")?;
    let due      = get_dt_str(todo.get_due(), "No due")?;
    let priority = todo.get_priority().map_err(Error::from)?.map(|p| p.as_str().to_string())
            .unwrap_or_else(|| "No prio".to_string());

    data.insert("uuid"     , uuid);
    data.insert("status"   , status);
    data.insert("text"     , text);
    data.insert("sched"    , sched);
    data.insert("hidden"   , hidden);
    data.insert("due"      , due);
    data.insert("priority" , priority);

    Ok(data)
}

