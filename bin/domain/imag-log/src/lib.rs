//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

#![forbid(unsafe_code)]

#![deny(
    non_camel_case_types,
    non_snake_case,
    path_statements,
    trivial_numeric_casts,
    unstable_features,
    unused_allocation,
    unused_import_braces,
    unused_imports,
    unused_must_use,
    unused_mut,
    unused_qualifications,
    while_true,
)]

extern crate clap;
#[macro_use] extern crate is_match;
#[macro_use] extern crate log;
extern crate toml;
extern crate toml_query;
extern crate itertools;
#[macro_use] extern crate anyhow;
extern crate textwrap;
extern crate resiter;

extern crate libimaglog;
extern crate libimagrt;
extern crate libimagstore;
extern crate libimagerror;
extern crate libimagdiary;

use std::io::Write;
use std::io::Cursor;
use std::str::FromStr;

use anyhow::Error;

use anyhow::Result;
use resiter::Map;
use resiter::AndThen;
use resiter::IterInnerOkOrElse;
use resiter::Filter;

use libimagrt::application::ImagApplication;
use libimagrt::runtime::Runtime;
use libimagdiary::diary::Diary;
use libimagdiary::diaryid::DiaryId;
use libimaglog::log::Log;
use libimagstore::iter::get::StoreIdGetIteratorExtension;
use libimagstore::store::FileLockEntry;

use clap::App;

mod ui;

use toml::Value;
use itertools::Itertools;

/// Marker enum for implementing ImagApplication on
///
/// This is used by binaries crates to execute business logic
/// or to build a CLI completion.
pub enum ImagLog {}
impl ImagApplication for ImagLog {
    fn run(rt: Runtime) -> Result<()> {
        if let Some(scmd) = rt.cli().subcommand_name() {
            match scmd {
                "show" => show(&rt),
                other  => {
                    debug!("Unknown command");
                    if rt.handle_unknown_subcommand("imag-bookmark", other, rt.cli())?.success() {
                        Ok(())
                    } else {
                        Err(anyhow!("Failed to handle unknown subcommand"))
                    }
                },
            }
        } else {
            let text       = get_log_text(&rt);
            let diary_name = match rt.cli().value_of("diaryname").map(String::from) {
                Some(s) => s,
                None => get_diary_name(&rt)?,
            };

            debug!("Writing to '{}': {}", diary_name, text);

            rt.store()
                .new_entry_now(&diary_name)
                .and_then(|mut fle| {
                    fle.make_log_entry()?;
                    *fle.get_content_mut() = text;
                    Ok(fle)
                })
                .and_then(|fle| rt.report_touched(fle.get_location()).map_err(Error::from))
        }
    }

    fn build_cli<'a>(app: App<'a, 'a>) -> App<'a, 'a> {
        ui::build_ui(app)
    }

    fn name() -> &'static str {
        env!("CARGO_PKG_NAME")
    }

    fn description() -> &'static str {
        "Overlay to imag-diary to 'log' single lines of text"
    }

    fn version() -> &'static str {
        env!("CARGO_PKG_VERSION")
    }
}

fn show(rt: &Runtime) -> Result<()> {
    use std::borrow::Cow;

    use libimagdiary::iter::DiaryEntryIterator;
    use libimagdiary::entry::DiaryEntry;

    let scmd = rt.cli().subcommand_matches("show").unwrap(); // safed by main()
    let iters : Vec<DiaryEntryIterator> = match scmd.values_of("show-name") {
        Some(values) => values
            .map(|diary_name| Diary::entries(rt.store(), diary_name))
            .collect::<Result<Vec<DiaryEntryIterator>>>(),

        None => if scmd.is_present("show-all") {
            debug!("Showing for all diaries");
            let iter = rt.store()
                .diary_names()?
                .map(|diary_name| {
                    let diary_name = diary_name?;
                    debug!("Getting entries for Diary: {}", diary_name);
                    let entries = Diary::entries(rt.store(), &diary_name)?;
                    let diary_name = Cow::from(diary_name);
                    Ok((entries, diary_name))
                })
                .collect::<Result<Vec<(DiaryEntryIterator, Cow<str>)>>>()?;

            let iter = iter.into_iter()
                .unique_by(|tpl| tpl.1.clone())
                .map(|tpl| tpl.0)
                .collect::<Vec<DiaryEntryIterator>>();

            Ok(iter)
        } else {
            // showing default logs
            get_diary_name(rt).and_then(|dname| Diary::entries(rt.store(), &dname)).map(|e| vec![e])
        }
    }?;

    let mut do_wrap = if scmd.is_present("show-wrap") {
        Some(80)
    } else {
        None
    };
    let do_remove_newlines = scmd.is_present("show-skipnewlines");

    if let Some(wrap_value) = scmd.value_of("show-wrap") {
        do_wrap = Some(usize::from_str(wrap_value).map_err(Error::from)?);
    }

    let mut output = rt.stdout();

    let v = iters.into_iter()
        .flatten()
        .into_get_iter(rt.store())
        .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
        .and_then_ok(|e| e.is_log().map(|b| (b, e)))
        .filter_ok(|tpl| tpl.0)
        .map_ok(|tpl| tpl.1)
        .and_then_ok(|entry| entry.diary_id().map(|did| (did.get_date_representation(), did, entry)))
        .collect::<Result<Vec<_>>>()?;

    v.into_iter()
        .sorted_by_key(|tpl| tpl.0)
        .map(|tpl| (tpl.1, tpl.2))
        .inspect(|tpl| debug!("Found entry: {:?}", tpl.1))
        .map(|(id, entry)| {
            if let Some(wrap_limit) = do_wrap {
                // assume a capacity here:
                // diaryname + year + month + day + hour + minute + delimiters + whitespace
                // 10 + 4 + 2 + 2 + 2 + 2 + 6 + 4 = 32
                // plus text, which we assume to be 120 characters... lets allocate 256 bytes.
                let mut buffer = Cursor::new(Vec::with_capacity(256));
                do_write_to(&mut buffer, id, &entry, do_remove_newlines)?;
                let buffer = String::from_utf8(buffer.into_inner())?;

                // now lets wrap
                ::textwrap::wrap(&buffer, wrap_limit)
                    .iter()
                    .map(|line| writeln!(&mut output, "{}", line).map_err(Error::from))
                    .collect::<Result<Vec<_>>>()?;
            } else {
                do_write_to(&mut output, id, &entry, do_remove_newlines)?;
            }

            rt.report_touched(entry.get_location()).map_err(Error::from)
        })
        .collect::<Result<Vec<_>>>()
        .map(|_| ())
}

fn get_diary_name(rt: &Runtime) -> Result<String> {
    use toml_query::read::TomlValueReadExt;
    use toml_query::read::TomlValueReadTypeExt;

    let cfg = rt
        .config()
        .ok_or_else(|| anyhow!("Configuration not present, cannot continue"))?;

    let current_log = cfg
        .read_string("log.default")?
        .ok_or_else(|| anyhow!("Configuration missing: 'log.default'"))?;

    if cfg
        .read("log.logs")?
        .ok_or_else(|| anyhow!("Configuration missing: 'log.logs'"))?
        .as_array()
        .ok_or_else(|| anyhow!("Configuration 'log.logs' is not an Array"))?
        .iter()
        .map(|e| if !is_match!(e, &Value::String(_)) {
            Err(anyhow!("Configuration 'log.logs' is not an Array<String>!"))
        } else {
            Ok(e)
        })
        .map_ok(|value| value.as_str().unwrap())
        .map_ok(String::from)
        .collect::<Result<Vec<_>>>()?
        .iter()
        .find(|log| *log == &current_log)
        .is_none()
    {
        Err(anyhow!("'log.logs' does not contain 'log.default'"))
    } else {
        Ok(current_log)
    }
}

fn get_log_text(rt: &Runtime) -> String {
    rt.cli()
        .values_of("text")
        .unwrap() // safe by clap
        .enumerate()
        .fold(String::with_capacity(500), |mut acc, (n, e)| {
            if n != 0 {
                acc.push_str(" ");
            }
            acc.push_str(e);
            acc
        })
}

fn do_write_to<'a>(sink: &mut dyn Write, id: DiaryId, entry: &FileLockEntry<'a>, do_remove_newlines: bool) -> Result<()> {
    let text = if do_remove_newlines {
        entry.get_content().trim_end().replace("\n", "")
    } else {
        entry.get_content().trim_end().to_string()
    };

    writeln!(sink,
            "{dname: >10} - {y: >4}-{m:0>2}-{d:0>2}T{H:0>2}:{M:0>2} - {text}",
             dname = id.diary_name(),
             y = id.year(),
             m = id.month(),
             d = id.day(),
             H = id.hour(),
             M = id.minute(),
             text = text)
        .map_err(Error::from)
}

