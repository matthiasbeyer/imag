//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::collections::BTreeMap;
use std::io::Write;

use clap::ArgMatches;
use anyhow::Error;
use anyhow::Result;

use handlebars::Handlebars;
use toml_query::read::TomlValueReadTypeExt;

use libimagmail::mail::Mail;
use libimagrt::runtime::Runtime;
use libimagstore::store::FileLockEntry;
use libimagentryref::reference::Config as RefConfig;

pub fn get_mail_print_format<'rc>(config_value_path: &'static str, rt: &Runtime, scmd: &ArgMatches) -> Result<Handlebars<'rc>> {
    let fmt = match scmd.value_of("format").map(String::from) {
        Some(s) => Ok(s),
        None => {
            rt.config()
                .ok_or_else(|| anyhow!("No configuration file"))?
                .read_string(config_value_path)
                .map_err(Error::from)?
                .ok_or_else(|| anyhow!("Configuration '{}' does not exist", config_value_path))
        }
    }?;

    let mut hb = Handlebars::new();
    hb.register_template_string("format", fmt)?;

    hb.register_escape_fn(::handlebars::no_escape);
    ::libimaginteraction::format::register_all_color_helpers(&mut hb);
    ::libimaginteraction::format::register_all_format_helpers(&mut hb);
    Ok(hb)
}

pub fn build_data_object_for_handlebars(i: usize, m: &FileLockEntry, refconfig: &RefConfig) -> Result<BTreeMap<&'static str, String>> {
    let mut data = BTreeMap::new();

    data.insert("i", format!("{}", i));

    let id = match m.get_message_id(refconfig)? {
        Some(f) => f.into(),
        None => "<no id>".to_owned(),
    };

    let from = match m.get_from(refconfig)? {
        Some(f) => f,
        None => "<no from>".to_owned(),
    };

    let to = match m.get_to(refconfig)? {
        Some(f) => f,
        None => "<no to>".to_owned(),
    };

    let subject = match m.get_subject(refconfig)? {
        Some(f) => f,
        None => "<no subject>".to_owned(),
    };

    data.insert("id"      , id);
    data.insert("from"    , from);
    data.insert("to"      , to);
    data.insert("subject" , subject);

    Ok(data)
}

pub fn list_mail(m: &FileLockEntry, i: usize, refconfig: &RefConfig, list_format: &Handlebars, out: &mut dyn Write) -> Result<()> {
    let data = build_data_object_for_handlebars(i, m, refconfig)?;
    let s    = list_format.render("format", &data)?;
    writeln!(out, "{}", s)?;
    Ok(())
}

