//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

#![forbid(unsafe_code)]

#![deny(
    non_camel_case_types,
    non_snake_case,
    path_statements,
    trivial_numeric_casts,
    unstable_features,
    unused_allocation,
    unused_import_braces,
    unused_imports,
    unused_must_use,
    unused_mut,
    unused_qualifications,
    while_true,
)]

extern crate clap;
#[macro_use] extern crate log;
#[macro_use] extern crate anyhow;
extern crate toml_query;
extern crate resiter;
extern crate handlebars;
extern crate walkdir;
extern crate rayon;

extern crate libimagrt;
extern crate libimagmail;
extern crate libimagerror;
extern crate libimagstore;
extern crate libimagutil;
extern crate libimagentryref;
extern crate libimaginteraction;

use std::path::PathBuf;

use anyhow::Result;

use anyhow::Error;
use toml_query::read::TomlValueReadTypeExt;
use clap::App;
use resiter::AndThen;
use resiter::Filter;
use resiter::IterInnerOkOrElse;
use resiter::Map;
use rayon::prelude::*;

use libimagmail::store::MailStore;
use libimagmail::mail::Mail;
use libimagentryref::util::get_ref_config;
use libimagentryref::reference::Config as RefConfig;
use libimagrt::runtime::Runtime;
use libimagrt::application::ImagApplication;
use libimagutil::info_result::*;
use libimagstore::storeid::StoreIdIterator;
use libimagstore::iter::get::StoreIdGetIteratorExtension;

mod ui;
mod util;

/// Marker enum for implementing ImagApplication on
///
/// This is used by binaries crates to execute business logic
/// or to build a CLI completion.
pub enum ImagMail {}
impl ImagApplication for ImagMail {
    fn run(rt: Runtime) -> Result<()> {
        match rt.cli().subcommand_name().ok_or_else(|| anyhow!("No subcommand called"))? {
            "scan"        => scan(&rt),
            "import-mail" => import_mail(&rt),
            "list"        => list(&rt),
            "unread"      => unread(&rt),
            "mail-store"  => mail_store(&rt),
            other               => {
                debug!("Unknown command");
                if rt.handle_unknown_subcommand("imag-mail", other, rt.cli())?.success() {
                    Ok(())
                } else {
                    Err(anyhow!("Failed to handle unknown subcommand"))
                }
            },
        }
    }

    fn build_cli<'a>(app: App<'a, 'a>) -> App<'a, 'a> {
        ui::build_ui(app)
    }

    fn name() -> &'static str {
        env!("CARGO_PKG_NAME")
    }

    fn description() -> &'static str {
        "Mail collection tool"
    }

    fn version() -> &'static str {
        env!("CARGO_PKG_VERSION")
    }
}

fn scan(rt: &Runtime) -> Result<()> {
    let collection_name = get_ref_collection_name(rt)?;
    let refconfig       = get_ref_config(rt, "imag-mail")?;
    let scmd            = rt.cli().subcommand_matches("scan").unwrap();

    /// Helper function to get an Iterator for all files from one PathBuf
    fn walk_pathes(path: PathBuf) -> impl Iterator<Item = Result<PathBuf>> {
        walkdir::WalkDir::new(path)
            .follow_links(false)
            .into_iter()
            .filter_ok(|entry| entry.file_type().is_file())
            .map_err(Error::from)
            .map_ok(|entry| entry.into_path())
            .inspect(|e| trace!("Processing = {:?}", e))
    }

    /// Helper function to process an iterator over Result<PathBuf> and create store entries for
    /// each path in the iterator
    fn process_iter(i: &mut dyn Iterator<Item = Result<PathBuf>>, rt: &Runtime, collection_name: &str, refconfig: &RefConfig) -> Result<()> {
        let scmd = rt.cli().subcommand_matches("scan").unwrap();
        i.and_then_ok(|path| {
                if scmd.is_present("ignore-existing-ids") {
                    rt.store().retrieve_mail_from_path(path, collection_name, refconfig, true)
                } else {
                    rt.store().create_mail_from_path(path, collection_name, refconfig)
                }
            })
            .and_then_ok(|e| rt.report_touched(e.get_location()).map_err(Error::from))
            .collect::<Result<Vec<_>>>()
            .map(|_| ())
    }

    let pathes = scmd.values_of("path")
        .unwrap() // enforced by clap
        .map(PathBuf::from)
        .collect::<Vec<_>>();

    if scmd.is_present("scan-parallel") {
        debug!("Fetching pathes in parallel");
        let mut i = pathes.into_par_iter()
            .map(|path| walk_pathes(path).collect::<Result<_>>())
            .collect::<Result<Vec<PathBuf>>>()?
            .into_iter()
            .map(Ok);

        debug!("Processing pathes");
        process_iter(&mut i, rt, &collection_name, &refconfig)
    } else {
        debug!("Fetching pathes not in parallel");
        let mut i = pathes
            .into_iter()
            .map(walk_pathes)
            .flatten();

        debug!("Processing pathes");
        process_iter(&mut i, rt, &collection_name, &refconfig)
    }
}

fn import_mail(rt: &Runtime) -> Result<()> {
    let collection_name = get_ref_collection_name(rt)?;
    let refconfig       = get_ref_config(rt, "imag-mail")?;
    let scmd            = rt.cli().subcommand_matches("import-mail").unwrap();
    let store           = rt.store();

    debug!(r#"Importing mail with
    collection_name = {}
    refconfig = {:?}
    "#, collection_name, refconfig);

    scmd.values_of("path")
        .unwrap() // enforced by clap
        .map(PathBuf::from)
        .map(|path| {
            if scmd.is_present("ignore-existing-ids") {
                store.retrieve_mail_from_path(path, &collection_name, &refconfig, true)
            } else {
                store.create_mail_from_path(path, &collection_name, &refconfig)
            }
            .map_info_str("Ok")
        })
        .and_then_ok(|e| rt.report_touched(e.get_location()).map_err(Error::from))
        .collect::<Result<Vec<()>>>()
        .map(|_| ())
}

fn list(rt: &Runtime) -> Result<()> {
    let refconfig   = get_ref_config(rt, "imag-mail")?;
    let scmd        = rt.cli().subcommand_matches("list").unwrap(); // safe via clap
    let list_format = util::get_mail_print_format("mail.list_format", rt, &scmd)?;
    let mut out     = rt.stdout();
    let mut i = 0;

    if rt.ids_from_stdin() {
        let iter = rt
            .ids::<crate::ui::PathProvider>()?
            .ok_or_else(|| anyhow!("No ids supplied"))?
            .into_iter()
            .map(Ok);

        StoreIdIterator::new(Box::new(iter))
    } else {
        rt.store()
            .all_mails()?
            .into_storeid_iter()
    }
    .inspect(|id| debug!("Found: {:?}", id))
    .into_get_iter(rt.store())
    .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
    .and_then_ok(|m| {
        crate::util::list_mail(&m, i, &refconfig, &list_format, &mut out)?;
        rt.report_touched(m.get_location())?;
        i += 1;
        Ok(())
    })
    .collect::<Result<Vec<_>>>()
    .map(|_| ())
}

fn unread(rt: &Runtime) -> Result<()> {
    let refconfig   = get_ref_config(rt, "imag-mail")?;
    let scmd        = rt.cli().subcommand_matches("unread").unwrap(); // safe via clap
    let list_format = util::get_mail_print_format("mail.list_format", rt, &scmd)?;
    let mut out     = rt.stdout();
    let mut i       = 0;

    if rt.ids_from_stdin() {
        let iter = rt
            .ids::<crate::ui::PathProvider>()?
            .ok_or_else(|| anyhow!("No ids supplied"))?
            .into_iter()
            .map(Ok);

        StoreIdIterator::new(Box::new(iter))
    } else {
        rt.store().all_mails()?.into_storeid_iter()
    }
    .inspect(|id| debug!("Found: {:?}", id))
    .into_get_iter(rt.store())
    .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
    .and_then_ok(|m| {
        if !m.is_seen(&refconfig)? {
            crate::util::list_mail(&m, i, &refconfig, &list_format, &mut out)?;
            rt.report_touched(m.get_location())?;
            i += 1;
        }

        Ok(())
    })
    .collect::<Result<Vec<_>>>()
    .map(|_| ())
}

fn mail_store(rt: &Runtime) -> Result<()> {
    let _ = rt.cli().subcommand_matches("mail-store").unwrap();
    Err(anyhow!("This feature is currently not implemented."))
}

fn get_ref_collection_name(rt: &Runtime) -> Result<String> {
    let setting_name = "mail.ref_collection_name";

    debug!("Getting configuration: {}", setting_name);

    rt.config()
        .ok_or_else(|| anyhow!("No configuration, cannot find collection name for mail collection"))?
        .read_string(setting_name)?
        .ok_or_else(|| anyhow!("Setting missing: {}", setting_name))
}

