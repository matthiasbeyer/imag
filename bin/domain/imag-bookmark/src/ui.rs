//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

use std::path::PathBuf;

use anyhow::Result;
use clap::{Arg, ArgMatches, App, SubCommand};

use libimagutil::cli_validators::*;
use libimagstore::storeid::StoreId;
use libimagstore::storeid::IntoStoreId;
use libimagrt::runtime::IdPathProvider;

pub fn build_ui<'a>(app: App<'a, 'a>) -> App<'a, 'a> {
    app
        .subcommand(SubCommand::with_name("add")
                   .about("Add bookmarks")
                   .version("0.1")
                   .arg(Arg::with_name("urls")
                        .index(1)
                        .takes_value(true)
                        .required(true)
                        .multiple(true)
                        .value_name("URL")
                        .validator(is_url)
                        .help("Add this URL(s)"))
                   )

        .subcommand(SubCommand::with_name("remove")
                   .about("Remove bookmarks")
                   .version("0.1")
                   .arg(Arg::with_name("ids")
                        .index(1)
                        .takes_value(true)
                        .required(true)
                        .multiple(true)
                        .value_name("ID")
                        .help("Remove these urls, specified as ID"))
                   )

        .subcommand(SubCommand::with_name("open")
                   .about("Open bookmarks")
                   .version("0.1")
                   .arg(Arg::with_name("ids")
                        .index(1)
                        .takes_value(true)
                        .required(false)
                        .multiple(true)
                        .value_name("ID")
                        .help("open these urls, specified as ID"))
                   .arg(Arg::with_name("opencmd")
                        .long("with")
                        .takes_value(true)
                        .required(false)
                        .multiple(false)
                        .value_name("spec")
                        .help("Open by executing this command, '{{url}}' will be replaced with the URL of the bookmark. Default is the setting bookmark.open in the configuration file."))
                   .arg(Arg::with_name("openparallel")
                        .long("parallel")
                        .takes_value(false)
                        .required(false)
                        .multiple(false)
                        .help("Open all links in parallel, don't open sequencially if opening commands blocks."))
                   )

        .subcommand(SubCommand::with_name("list")
                   .about("List bookmarks, if used in pipe, ignores everything that is not a bookmark")
                   .version("0.1")

                   .arg(Arg::with_name("ids")
                        .index(1)
                        .takes_value(true)
                        .required(false)
                        .multiple(true)
                        .value_name("ID")
                        .help("IDs of bookmarks to list"))
                   )

        .subcommand(SubCommand::with_name("find")
                   .about("Find a bookmark by substring of URL")
                   .version("0.1")

                   .arg(Arg::with_name("substr")
                        .index(1)
                        .takes_value(true)
                        .required(true)
                        .multiple(false)
                        .value_name("str")
                        .help("Substring to search in the URL."))

                   .arg(Arg::with_name("ids")
                        .index(2)
                        .takes_value(true)
                        .required(false)
                        .multiple(true)
                        .value_name("IDs")
                        .help("IDs to search in (if not passed, searches all bookmarks. Can also be provided via STDIN"))
                   )
}

pub struct PathProvider;
impl IdPathProvider for PathProvider {
    fn get_ids(matches: &ArgMatches) -> Result<Option<Vec<StoreId>>> {
        fn no_ids_error() -> Result<Option<Vec<StoreId>>> {
            Err(anyhow!("Command does not get IDs as input"))
        }

        fn get_id_paths(field: &str, subm: &ArgMatches) -> Result<Option<Vec<StoreId>>> {
            subm.values_of(field)
                .map(|v| v
                     .map(PathBuf::from)
                     .map(|pb| pb.into_storeid())
                     .collect::<Result<Vec<_>>>()
                )
                .transpose()
        }

        match matches.subcommand() {
            ("add", _) => no_ids_error(),
            ("open", Some(subm)) => get_id_paths("ids", subm),
            ("remove", Some(subm)) => get_id_paths("ids", subm),
            ("list", Some(subm)) => get_id_paths("ids", subm),
            ("find", Some(subm)) => get_id_paths("ids", subm),
            (other, _) => Err(anyhow!("Not a known command: {}", other)),
        }
    }
}
