//
// imag - the personal information management suite for the commandline
// Copyright (C) 2015-2020 Matthias Beyer <mail@beyermatthias.de> and contributors
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; version
// 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//

#![forbid(unsafe_code)]

#![deny(
    non_camel_case_types,
    non_snake_case,
    path_statements,
    trivial_numeric_casts,
    unstable_features,
    unused_allocation,
    unused_import_braces,
    unused_imports,
    unused_must_use,
    unused_mut,
    unused_qualifications,
    while_true,
)]

extern crate clap;
#[macro_use] extern crate log;
extern crate toml;
extern crate url;
extern crate uuid;
extern crate toml_query;
#[macro_use] extern crate anyhow;
extern crate resiter;
extern crate handlebars;
extern crate rayon;

extern crate libimagbookmark;
extern crate libimagrt;
extern crate libimagerror;
extern crate libimagstore;
extern crate libimagutil;
extern crate libimagentryurl;

use std::io::Write;
use std::collections::BTreeMap;
use std::process::Command;

use anyhow::Error;

use anyhow::Result;
use resiter::AndThen;
use resiter::IterInnerOkOrElse;
use clap::App;
use url::Url;
use handlebars::Handlebars;
use rayon::iter::ParallelIterator;
use rayon::iter::IntoParallelIterator;
use toml_query::read::TomlValueReadExt;

use libimagrt::runtime::Runtime;
use libimagrt::application::ImagApplication;
use libimagstore::iter::get::StoreIdGetIteratorExtension;
use libimagstore::store::FileLockEntry;
use libimagbookmark::store::BookmarkStore;
use libimagbookmark::bookmark::Bookmark;
use libimagentryurl::link::Link;


mod ui;

/// Marker enum for implementing ImagApplication on
///
/// This is used by binaries crates to execute business logic
/// or to build a CLI completion.
pub enum ImagBookmark {}
impl ImagApplication for ImagBookmark {
    fn run(rt: Runtime) -> Result<()> {
        match rt.cli().subcommand_name().ok_or_else(|| anyhow!("No subcommand called"))? {
            "add"        => add(&rt),
            "open"       => open(&rt),
            "list"       => list(&rt),
            "remove"     => remove(&rt),
            "find"       => find(&rt),
            other        => {
                debug!("Unknown command");
                if rt.handle_unknown_subcommand("imag-bookmark", other, rt.cli())?.success() {
                    Ok(())
                } else {
                    Err(anyhow!("Failed to handle unknown subcommand"))
                }
            },
        }
    }

    fn build_cli<'a>(app: App<'a, 'a>) -> App<'a, 'a> {
        ui::build_ui(app)
    }

    fn name() -> &'static str {
        env!("CARGO_PKG_NAME")
    }

    fn description() -> &'static str {
        "Bookmark collection tool"
    }

    fn version() -> &'static str {
        env!("CARGO_PKG_VERSION")
    }
}

fn add(rt: &Runtime) -> Result<()> {
    let scmd = rt.cli().subcommand_matches("add").unwrap();
    scmd.values_of("urls")
        .unwrap()
        .map(|s| Url::parse(s).map_err(Error::from))
        .and_then_ok(|url| {
            let (uuid, fle) = rt.store().add_bookmark(url.clone())?;
            debug!("Created entry for url '{}' with uuid '{}'", url, uuid);
            info!("{} = {}", url, uuid);
            rt.report_touched(fle.get_location()).map_err(Error::from)
        })
        .collect()
}

fn open(rt: &Runtime) -> Result<()> {
    let scmd = rt.cli().subcommand_matches("open").unwrap();
    let open_command = rt.config()
        .map(|value| {
            value.read("bookmark.open")?
                .ok_or_else(|| anyhow!("Configuration missing: 'bookmark.open'"))?
                .as_str()
                .ok_or_else(|| anyhow!("Open command should be a string"))
        })
        .or_else(|| Ok(scmd.value_of("opencmd")).transpose())
        .unwrap_or_else(|| Err(anyhow!("No open command available in config or on commandline")))?;

    let hb = {
        let mut hb = Handlebars::new();
        hb.register_template_string("format", open_command)?;
        hb
    };

    let iter = rt.ids::<crate::ui::PathProvider>()?
        .ok_or_else(|| anyhow!("No ids supplied"))?
        .into_iter()
        .map(Ok)
        .into_get_iter(rt.store())
        .map_inner_ok_or_else(|| anyhow!("Did not find one entry"));

    if scmd.is_present("openparallel") {
        let links = iter
            .and_then_ok(|link| rt.report_touched(link.get_location()).map_err(Error::from).map(|_| link))
            .and_then_ok(|link| calculate_command_data(&hb, &link, open_command))
            .collect::<Result<Vec<_>>>()?;

        links
            .into_par_iter()
            .map(|command_rendered| {
                    Command::new(&command_rendered[0]) // indexing save with check above
                        .args(&command_rendered[1..])
                        .output()
                        .map_err(Error::from)
            })
            .collect::<Result<Vec<_>>>()
            .map(|_| ())
    } else {
        iter.and_then_ok(|link| {
                rt.report_touched(link.get_location()).map_err(Error::from).map(|_| link)
            })
            .and_then_ok(|link| {
                let command_rendered = calculate_command_data(&hb, &link, open_command)?;
                Command::new(&command_rendered[0]) // indexing save with check above
                    .args(&command_rendered[1..])
                    .output()
                    .map_err(Error::from)
            })
            .collect::<Result<Vec<_>>>()
            .map(|_| ())
    }
}

fn list(rt: &Runtime) -> Result<()> {
    rt.store()
        .all_bookmarks()?
        .into_get_iter()
        .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
        .and_then_ok(|entry| {
            if entry.is_bookmark()? {
                let url = entry.get_url()?
                    .ok_or_else(|| anyhow!("Failed to retrieve URL for {}", entry.get_location()))?;
                if !rt.output_is_pipe() {
                    writeln!(rt.stdout(), "{}", url)?;
                }

                rt.report_touched(entry.get_location()).map_err(Error::from)
            } else {
                Ok(())
            }
        })
        .collect()
}

fn remove(rt: &Runtime) -> Result<()> {
    rt.ids::<crate::ui::PathProvider>()?
        .ok_or_else(|| anyhow!("No ids supplied"))?
        .into_iter()
        .map(Ok)
        .into_get_iter(rt.store())
        .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
        .and_then_ok(|fle| {
            rt.report_touched(fle.get_location())
                .map_err(Error::from)
                .and_then(|_| rt.store().remove_bookmark(fle))
        })
        .collect()
}

fn find(rt: &Runtime) -> Result<()> {
    let substr = rt.cli().subcommand_matches("find").unwrap().value_of("substr").unwrap();

    if let Some(ids) = rt.ids::<crate::ui::PathProvider>()? {
        ids.into_iter()
            .map(Ok)
            .into_get_iter(rt.store())
    } else {
        rt.store()
            .all_bookmarks()?
            .into_get_iter()
    }
    .map_inner_ok_or_else(|| anyhow!("Did not find one entry"))
    .and_then_ok(|fle| {
        if fle.is_bookmark()? {
            let url = fle
                .get_url()?
                .ok_or_else(|| anyhow!("Failed to retrieve URL for {}", fle.get_location()))?;
            if url.as_str().contains(substr) {
                if !rt.output_is_pipe() {
                    writeln!(rt.stdout(), "{}", url)?;
                }
                rt.report_touched(fle.get_location()).map_err(Error::from)
            } else {
                Ok(())
            }
        } else {
            Ok(())
        }
    })
    .collect()
}

fn calculate_command_data<'a>(hb: &Handlebars, entry: &FileLockEntry<'a>, open_command: &str) -> Result<Vec<String>> {
    let url = entry.get_url()?
        .ok_or_else(|| anyhow!("Failed to retrieve URL for {}", entry.get_location()))?
        .into_string();

    let data = {
        let mut data = BTreeMap::new();
        data.insert("url", url);
        data
    };

    let command_rendered = hb.render("format", &data)?
        .split_whitespace()
        .map(String::from)
        .collect::<Vec<String>>();

    if command_rendered.len() > 2 {
        return Err(anyhow!("Command seems not to include URL: '{}'", open_command));
    }

    Ok(command_rendered.into_iter().map(String::from).collect())
}
